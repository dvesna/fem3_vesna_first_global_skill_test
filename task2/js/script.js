const pinInput = document.getElementById('pin-code');
const moneySum = document.getElementById('money-sum');


let userCard = {
  deposit: 10000,
  pin: 2233,
  maxPinInput: 3,
    status: 'active',
  getCash: (pin = pinInput.value, money = moneySum.value) => {
    pin !== userCard.pin ? userCard.maxPinInput = 3 : userCard.maxPinInput -= 1;
    if (userCard.maxPinInput < 1) {
        userCard.status = 'disabled';
        alert('Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки');
        pinInput.disabled = true;
        moneySum.disabled = true;
    }
  }
};

