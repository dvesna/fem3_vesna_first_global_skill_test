const englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы", "жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];

/* в результате массив englishBreakfast должен быть равен:
englishBreakfast = ["поджареный бекон", "колбаски", "яичница", "жареные грибы"];

и должен появится еще массив dinner, со значениями:
dinner = ["жареные помидоры", "фасоль", "хлеб с джемом", "кофе"];
*/

let breakfast = englishBreakfast.slice(0, 4);
let dinner = englishBreakfast.slice(4, englishBreakfast.length);

console.log(breakfast, dinner);